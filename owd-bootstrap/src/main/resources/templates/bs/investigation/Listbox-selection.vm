###############################################################################
##  |	Copyright 2014 Monad Solutions Ltd.
##  |
##  |	Licensed under the Apache License, Version 2.0 (the "License");
##  |	you may not use this file except in compliance with the License.
##  |	You may obtain a copy of the License at
##  |
##  |		http://www.apache.org/licenses/LICENSE-2.0
##  |
##  |	Unless required by applicable law or agreed to in writing, software
##  |	distributed under the License is distributed on an "AS IS" BASIS,
##  |	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
##  |	See the License for the specific language governing permissions and
##  |	limitations under the License.
###############################################################################

#set( $listValues = ${control.getListOptions()} )

#if(${control.isReadOnly()})
    #set($readOnlyString = "disabled")
#else
    #set($readOnlyString = "")
#end

<select id="${control.getEncodedID()}" name="${control.getId()}" $readOnlyString size="${listValues.size()}" tabindex="#tabIndex()" ${styleAttribute} ${classAttribute} >
    #foreach( $listValue in $listValues )
        #if(${listValue.isVisible()})
            #if( ${control.getDisplayValue()} == ${listValue.getValue()} )
               <option selected value="${listValue.getValue()}">${listValue.getDisplayText()}</option>
            #elseif ($control.getControlType().equals("BooleanInputControl"))
                #if ($control.isTrue() && $listValue.getValue().equals(${boolean-true}))
                        <option selected value="${listValue.getValue()}">${listValue.getDisplayText()}</option>
                #elseif ($control.isFalse() && $listValue.getValue().equals(${boolean-false}))
                        <option selected value="${listValue.getValue()}">${listValue.getDisplayText()}</option>
                #else
                        <option value="${listValue.getValue()}">${listValue.getDisplayText()}</option>
                #end
           #else
               <option value="${listValue.getValue()}">${listValue.getDisplayText()}</option>
           #end
        #end
    #end
</select>