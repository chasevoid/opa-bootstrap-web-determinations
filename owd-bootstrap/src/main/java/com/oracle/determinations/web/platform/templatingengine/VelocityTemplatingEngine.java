/******************************************************************************** 
 *	Copyright 2014 Monad Solutions Ltd.
 *
 *  	Licensed under the Apache License, Version 2.0 (the "License");
 *	you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *		http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *	distributed under the License is distributed on an "AS IS" BASIS,
 *	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *	See the License for the specific language governing permissions and
 *	limitations under the License.
********************************************************************************/

package com.oracle.determinations.web.platform.templatingengine;

import com.oracle.determinations.web.platform.datamodel.template.AbstractTemplate;
import com.oracle.determinations.web.platform.datamodel.template.ScreenTemplate;
import com.oracle.determinations.web.platform.datamodel.template.StyleTemplate;
import com.oracle.determinations.web.platform.datamodel.template.VelocityTemplateContext;
import com.oracle.determinations.web.platform.exceptions.WebDeterminationsException;
import com.oracle.determinations.web.platform.exceptions.error.MissingResourceError;
import com.oracle.determinations.web.platform.exceptions.error.ResourceLoadError;
import com.oracle.determinations.web.platform.exceptions.error.TemplateParseError;
import com.oracle.determinations.web.platform.servlet.WebDeterminationsServletConfiguration;
import com.oracle.determinations.web.platform.servlet.WebDeterminationsServletContext;
import java.io.StringWriter;
import java.util.Properties;
import org.apache.log4j.Logger;
import org.apache.velocity.Template;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.exception.ResourceNotFoundException;

public final class VelocityTemplatingEngine implements TemplatingEngine {
	private VelocityEngine engine;
	private static final Logger logger = Logger
			.getLogger(VelocityTemplatingEngine.class);

	public VelocityTemplatingEngine(WebDeterminationsServletContext context) {
		String resourceLoaderClass;
		String resourceLoader;

		if (context.getConfiguration().isLoadTemplatesAsResource()) {
			resourceLoader = "classpath";
			resourceLoaderClass = "com.oracle.determinations.web.platform.templatingengine.VelocityTemplateClasspathLoader";
		} else {
			resourceLoader = "file";
			resourceLoaderClass = "org.apache.velocity.runtime.resource.loader.FileResourceLoader";

		}

		Class c = VelocityTemplatingEngine.class;

		Properties props = new Properties();
		props.put("resource.loader", resourceLoader);
		props.put("runtime.log.logsystem.class",
				"org.apache.velocity.runtime.log.Log4JLogChute");
		props.put("runtime.log.logsystem.log4j.logger", c.getName());
		props.put(resourceLoader + ".resource.loader.class",
				resourceLoaderClass);
		props.put(resourceLoader + ".resource.loader.path", context
				.getConfiguration().getTemplatesPath());
		props.put(resourceLoader + ".resource.loader.cache", "false");
		props.put("input.encoding", "UTF-8");
		props.put("output.encoding", "UTF-8");
		props.put("directive.parse.max.depth", "0");
//		props.put("velocimacro.max.depth", "0");
		props.put("velocimacro.max.depth", "10");
System.err.println("SETTING velocimacro.max.depth = 10 !");

		if (context.getConfiguration().isCacheTemplates()) {
			props.put(resourceLoader + ".resource.loader.cache", "true");
			props.put(resourceLoader
					+ ".resource.loader.modificationCheckInterval", "0");
			props.put("resource.manager.defaultcache.size", "0");
		} else {
			props.put(resourceLoader + ".resource.loader.cache", "false");

		}

		try {
			this.engine = new VelocityEngine();
			this.engine.init(props);
		} catch (Exception e) {
			throw new WebDeterminationsException(
					"Could not initialise Velocity Templating Engine", e);
		}
	}

	public AbstractTemplate getTemplate(String templateName) {
		Template velocityTemplate;
		try {
			velocityTemplate = this.engine.getTemplate(templateName);
		} catch (ResourceNotFoundException ex) {
			throw new MissingResourceError(templateName, ex);
		} catch (Exception ex) {
			throw new ResourceLoadError(templateName, ex);

		}

		if (ScreenTemplate.isScreenTemplate(templateName))
			return new ScreenTemplate(velocityTemplate);
		if (StyleTemplate.isStyleTemplate(templateName)) {
			return new StyleTemplate(velocityTemplate);
		}
		throw new TemplateParseError(templateName);
	}

	public String mergeTemplate(AbstractTemplate template,
			VelocityTemplateContext context) {
		StringWriter writer = new StringWriter();
		try {
			template.getTemplate().merge(context.getContext(), writer);
		} catch (Exception ex) {
			logger.error(
					"Error processing template:'" + template.getTemplateName()
							+ "'", ex);
			throw new TemplateParseError(template.getTemplateName(), ex);
		}
		return writer.toString();
	}

	public String evaluate(VelocityTemplateContext context, String template) {
		StringWriter sw = new StringWriter();
		try {
			this.engine.evaluate(context.getContext(), sw, "EvaluatedString",
					template);
		} catch (Exception ex) {
			logger.error("Error evaluating template: '" + template + "'", ex);
			throw new TemplateParseError("EvaluatedString", ex);
		}

		return sw.toString();
	}
}
