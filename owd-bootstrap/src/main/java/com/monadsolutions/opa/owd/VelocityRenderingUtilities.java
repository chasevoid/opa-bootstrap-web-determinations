/******************************************************************************** 
 *	Copyright 2014 Monad Solutions Ltd.
 *
 *  	Licensed under the Apache License, Version 2.0 (the "License");
 *	you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *		http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *	distributed under the License is distributed on an "AS IS" BASIS,
 *	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *	See the License for the specific language governing permissions and
 *	limitations under the License.
********************************************************************************/

package com.monadsolutions.opa.owd;

import java.text.NumberFormat;
import java.util.List;

import org.apache.velocity.VelocityContext;

import com.oracle.determinations.web.platform.datamodel.screen.NativeScreen;
import com.oracle.determinations.web.platform.datamodel.screen.controls.Control;
import com.oracle.determinations.web.platform.datamodel.screen.controls.ControlContainer;
import com.oracle.determinations.web.platform.datamodel.screen.controls.GroupControl;
import com.oracle.determinations.web.platform.eventmodel.events.OnApplyTemplatesEvent;
import com.oracle.determinations.web.platform.eventmodel.handlers.OnApplyTemplatesHandler;
import com.oracle.determinations.web.platform.plugins.PlatformSessionPlugin;
import com.oracle.determinations.web.platform.plugins.PlatformSessionRegisterArgs;

/**
 * Methods to help when rendering using Velocity.
 *  
 * @author Luke Studley
 *
 */
public class VelocityRenderingUtilities implements OnApplyTemplatesHandler {

	public PlatformSessionPlugin getInstance(PlatformSessionRegisterArgs arg0) {
		return this;
	}

	public class TemplateContextHelper {

		private VelocityContext velocityContext;

		public TemplateContextHelper(VelocityContext context) {
			this.velocityContext = context;
		}

		public NativeScreen getScreen() {
			return (NativeScreen) velocityContext.get("screen");
		}
		
		/**
		 * 
		 * @return True if this screen or any control has any messages
		 */
		public boolean getScreenHasMessages( NativeScreen scr ) {
			return ! scr.getWarnings().isEmpty() || ! scr.getErrors().isEmpty();
		}

		public boolean getControlHasMessages( Control ctrl ) {
			boolean hasMessages = ! ctrl.getWarningMessages().isEmpty() || ! ctrl.getErrorMessages().isEmpty();
			if ( ! hasMessages && ctrl instanceof GroupControl ) {
				hasMessages = getControlContainerHasMessages((ControlContainer) ctrl);
			}
			return hasMessages;
		}

		public boolean getControlContainerHasMessages(ControlContainer container) {
			
			boolean hasMessages = false;
			
			if ( container instanceof NativeScreen ) {
				hasMessages = getScreenHasMessages((NativeScreen) container);
			}
			else if ( container instanceof Control ) {
				Control ctrl = (Control) container;
				hasMessages = ! ctrl.getWarningMessages().isEmpty() || ! ctrl.getErrorMessages().isEmpty();
			}

			if ( ! hasMessages ) {
				// Check sub controls
				for ( Control subCtrl : ((List<Control>) container.getControls() ) ) {
					hasMessages = getControlHasMessages(subCtrl);
					if ( hasMessages ) break;
				}
			}
	
			return hasMessages;
		}
		
		public String formatPercent(double number) {
			return NumberFormat.getPercentInstance().format(Math.round(number) / 100.0);
		}
		
		 
	}
	
	public void handleEvent(Object arg0, OnApplyTemplatesEvent ev) {
		
		ev.getTemplateContext().getContext().put("contextHelper", new TemplateContextHelper(ev.getTemplateContext().getContext()));
		
		
	}
	
	
}
